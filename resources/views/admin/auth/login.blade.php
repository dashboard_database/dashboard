<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="csrf-token" content="#">

        <title>Admin | Login</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Public+Sans:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.css" rel="stylesheet">

        <!-- Icons -->
        <link href="{{ asset('assets/css/nucleo-icons.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="{{asset('assets/css/izitoast/css/iziToast.min.css')}}">
        <!-- CSS -->
        <link href="{{ asset('assets/css/white-dashboard.css?v=1.0.0')}}" rel="stylesheet" />
        <link href="{{ asset('assets/css/theme.css')}}" rel="stylesheet" />
    </head>
    <body class="white-content">
            <div class="wrapper">
                <div class="main-panel">
                    <div class="content">
                        <div class="col-lg-4 col-md-6 ml-auto mr-auto">
                            <form class="form" method="post" action="{{ route('login') }}">
                                @csrf

                                <div class="card card-login card-white">
                                    <div class="card-header">
                                        <img src="{{ asset('assets/img/card-primary.png') }}" alt="">
                                        <h1 class="card-title">Login</h1>
                                    </div>
                                    <div class="card-body">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="tim-icons icon-email-85"></i>
                                                </div>
                                            </div>
                                            <input type="email" name="email" class="form-control" placeholder="Email">
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="tim-icons icon-lock-circle"></i>
                                                </div>
                                            </div>
                                            <input type="password" placeholder="Password" name="password" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group mt-2 ml-4">
                                        <input type="checkbox" name="remember" id="checkbox-signin" value="">
                                        <label>Remember me</label>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" href="" class="btn btn-primary btn-lg btn-block mb-3">Log in</button>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <script src="{{ asset('assets/js/core/jquery.min.js')}}"></script>
            <script src="{{ asset('assets/js/core/popper.min.js')}}"></script>
            <script src="{{ asset('assets/js/core/bootstrap.min.js')}}"></script>
            <script src="{{ asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
            <!-- Chart JS -->
            {{-- <script src="{{ asset('assets/js/plugins/chartjs.min.js"></script> --}}
            <!--  Notifications Plugin    -->
            {{-- <script src="{{ asset('assets/js/plugins/bootstrap-notify.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets/js/plugins/chartjs.min.js') }}"></script> --}}
            <script src="{{ asset('assets/js/white-dashboard.min.js?v=1.0.0')}}"></script>
            <script src="{{ asset('assets/js/theme.js')}}"></script>

            <script src="{{asset('assets/css/izitoast/js/iziToast.min.js')}}"></script>
            <script>
                @if(Session::has('success'))
                    iziToast.success({
                    title: '{{ session('success') }}',
                    position: 'topRight'
                });
                @endif
                @if(Session::has('error'))
                    iziToast.error({
                    title: '{{ session('error') }}',
                    position: 'topRight'
                });
                @endif
            </script>

    </body>
</html>



