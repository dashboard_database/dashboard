@extends('admin.layout.master')
@section('container')
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <div class="row">
                    <div class="col-8">
                        <h4>Contact</h4>
                    </div>
                    <div class="col-4 text-right">
                        @include('admin.layout.button')
                        {{-- <a href="#" class="btn btn-sm btn-primary">{{ __('Add user') }}</a> --}}
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="ExportdataTable">
                        <thead class=" text-primary">
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Subject</th>
                            <th>Message</th>
                            <th>Created Date</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <?php $j=1 ?>
                            @foreach ($contacts as $contact)
                                <tr>
                                    <td>{{ $j++ }}</td>
                                    <td>{{ $contact->name }}</td>
                                    <td><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></td>
                                    <td>{{ $contact->contact }}</td>
                                    <td>{{ $contact->subject }}</td>
                                    <td>{{ $contact->message }}</td>
                                    <td>{{ $contact->created_at->format('d/m/Y H:i') }}</td>
                                    <td>
                                        <form action="{{route('contact.destroy',$contact->id)}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="bottom" title="Delete Contact" onclick="confirm('Are you sure you want to delete this sale? All your records will be permanently deleted.') ? this.parentElement.submit() : ''">
                                                <i class="tim-icons icon-simple-remove"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
