


<!DOCTYPE html>
<html lang="en">
    @include('admin.layout.head')

    @include('admin.layout.sidebar')
    @include('admin.layout.nav')

    <body class="white-content">
        {{-- @auth() --}}
            <div class="wrapper">
                <div class="main-panel">

                    <div class="content">
                        @section('container')

                        @show
                    </div>

                </div>
            </div>
            {{-- <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form> --}}
        {{-- @else
            @include('layouts.navbars.navbar')
            <div class="wrapper wrapper-full-page">
                <div class="full-page {{ $contentClass ?? '' }}">
                    <div class="content">
                        <div class="container">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        @endauth --}}

    @include('admin.layout.script')

    </body>
</html>
