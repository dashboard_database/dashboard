        <script src="{{ asset('assets/js/core/jquery.min.js')}}"></script>
        <script src="{{ asset('assets/js/core/popper.min.js')}}"></script>
        <script src="{{ asset('assets/js/core/bootstrap.min.js')}}"></script>
        <script src="{{ asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
        <!-- Chart JS -->
        {{-- <script src="{{ asset('assets/js/plugins/chartjs.min.js"></script> --}}
        <!--  Notifications Plugin    -->
        <script type="text/javascript" src="{{asset('assets/js/datatable/datatables.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/datatable/pdfmake.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/datatable/vfs_fonts.js')}}"></script>

        <script src="{{ asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
        <script src="{{ asset('assets/js/plugins/chartjs.min.js') }}"></script>
        <script src="{{ asset('assets/js/white-dashboard.min.js?v=1.0.0')}}"></script>
        <script src="{{ asset('assets/js/theme.js')}}"></script>
        <script src="{{asset('assets/css/izitoast/js/iziToast.min.js')}}"></script>
            <script>
                @if(Session::has('success'))
                    iziToast.success({
                    title: '{{ session('success') }}',
                    position: 'topRight'
                });
                @endif
                @if(Session::has('error'))
                    iziToast.error({
                    title: '{{ session('error') }}',
                    position: 'topRight'
                });
                @endif
            </script>
        <script>
            $(document).ready(function() {
                $().ready(function() {
                    $sidebar = $('.sidebar');
                    $navbar = $('.navbar');
                    $main_panel = $('.main-panel');

                    $full_page = $('.full-page');

                    $sidebar_responsive = $('body > .navbar-collapse');
                    sidebar_mini_active = true;
                    white_color = false;

                    window_width = $(window).width();

                    fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

                    $('.fixed-plugin a').click(function(event) {
                        if ($(this).hasClass('switch-trigger')) {
                            if (event.stopPropagation) {
                                event.stopPropagation();
                            } else if (window.event) {
                                window.event.cancelBubble = true;
                            }
                        }
                    });

                    $('.fixed-plugin .background-color span').click(function() {
                        $(this).siblings().removeClass('active');
                        $(this).addClass('active');

                        var new_color = $(this).data('color');

                        if ($sidebar.length != 0) {
                            $sidebar.attr('data', new_color);
                        }

                        if ($main_panel.length != 0) {
                            $main_panel.attr('data', new_color);
                        }

                        if ($full_page.length != 0) {
                            $full_page.attr('filter-color', new_color);
                        }

                        if ($sidebar_responsive.length != 0) {
                            $sidebar_responsive.attr('data', new_color);
                        }
                    });

                    $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
                        var $btn = $(this);

                        if (sidebar_mini_active == true) {
                            $('body').removeClass('sidebar-mini');
                            sidebar_mini_active = false;
                            whiteDashboard.showSidebarMessage('Sidebar mini deactivated...');
                        } else {
                            $('body').addClass('sidebar-mini');
                            sidebar_mini_active = true;
                            whiteDashboard.showSidebarMessage('Sidebar mini activated...');
                        }

                        // we simulate the window Resize so the charts will get updated in realtime.
                        var simulateWindowResize = setInterval(function() {
                            window.dispatchEvent(new Event('resize'));
                        }, 180);

                        // we stop the simulation of Window Resize after the animations are completed
                        setTimeout(function() {
                            clearInterval(simulateWindowResize);
                        }, 1000);
                    });

                    $('.switch-change-color input').on("switchChange.bootstrapSwitch", function() {
                            var $btn = $(this);

                            if (white_color == true) {
                                $('body').addClass('change-background');
                                setTimeout(function() {
                                    $('body').removeClass('change-background');
                                    $('body').removeClass('white-content');
                                }, 900);
                                white_color = false;
                            } else {
                                $('body').addClass('change-background');
                                setTimeout(function() {
                                    $('body').removeClass('change-background');
                                    $('body').addClass('white-content');
                                }, 900);

                                white_color = true;
                            }
                    });

                    $('.light-badge').click(function() {
                        $('body').addClass('white-content');
                    });

                    $('.dark-badge').click(function() {
                        $('body').removeClass('white-content');
                    });
                });
            });
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <script>
            // datatable
            //start data export table
            var contacttable = $('#ExportdataTable').DataTable({
                language: {
                    paginate: {
                    next: '>', // '&#8594;' or '→'
                    previous: '<', //'&#8592;' or '←'
                    }
                },
                buttons: [
                    'print',
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5',
                ],
            });


            $('#export_print').on('click', function(e) {
                e.preventDefault();
                contacttable.button(0).trigger();

            });


            $('#export_excel').on('click', function(e) {
                e.preventDefault();
                contacttable.button(2).trigger();

            });

            $('#export_pdf').on('click', function(e) {
                e.preventDefault();
                contacttable.button(4).trigger();

            });
            //end data export table
        // End datatable
        </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.js"></script>
