<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Docter</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Public+Sans:wght@100;200;300;400;500;600&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.css" rel="stylesheet">

    <!-- Icons -->
    <link href="{{ asset('assets/css/nucleo-icons.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('assets/css/izitoast/css/iziToast.min.css')}}">
    <link  rel="stylesheet" href="{{ asset('assets/css/datatable/datatables.min.css')}}" type="text/css"/>
    <!-- CSS -->
    <link href="{{ asset('assets/css/white-dashboard.css?v=1.0.0')}}" rel="stylesheet" />
    <link href="{{ asset('assets/css/theme.css')}}" rel="stylesheet" />
</head>
