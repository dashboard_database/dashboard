<div class="btn-group export-btns">
    <button type="button" class="btn btn-sm btn-primary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-download"></i> Export </button>
    <div class="dropdown-menu">
            <a class="dropdown-item text-dark" id="export_print">
                <span class="navi-icon">
                    <i class="fa fa-print"></i>
                </span>
                <span class="navi-text ml-2">{{__('Print')}}</span>
            </a>

            <a class="dropdown-item text-success" id="export_excel">
                <span class="navi-icon">
                    <i class="fa fa-file-excel-o"></i>
                </span>
                <span class="navi-text ml-2">{{__('Excel')}}</span>
            </a>

            <a class="dropdown-item text-danger" id="export_pdf">
                <span class="navi-icon">
                    <i class="fa fa-file-pdf-o"></i>
                </span>
                <span class="navi-text ml-2">{{__('PDF')}}</span>
            </a>
    </div>
</div>

<style>
    .dt-buttons{
        display: none;
    }
</style>
