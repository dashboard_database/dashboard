@extends('admin.layout.master')
@section('container')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4>Edit Profile</h4>
                </div>
                <form method="post" action="{{route('admin-profile-update',$profile->id)}}" enctype="multipart/form-data" autocomplete="off">
                    <div class="card-body">
                            @csrf
                            {{-- <div class="form-group"> --}}
                                <label>Profile Image</label>
                                <input type="file" name="image" class="form-control" value="{{ old('image', $profile->image) }}">
                                @error('image')
                                <span class="text-danger"> *{{$message}}</span>
                                @enderror
                            {{-- </div> --}}
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" placeholder="Name" value="{{ old('name', $profile->name) }}">
                                @error('name')
                                <span class="text-danger"> *{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email', $profile->email) }}">
                                @error('email')
                                    <span class="text-danger"> *{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Contact</label>
                                <input type="number" name="contact" class="form-control" placeholder="Contact" value="{{ old('contact', $profile->contact) }}">
                                @error('contact')
                                    <span class="text-danger"> *{{$message}}</span>
                                @enderror
                            </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-user">
                <div class="card-body">
                    <p class="card-text">
                        <div class="author">
                            <div class="block block-one"></div>
                            <div class="block block-two"></div>
                            <div class="block block-three"></div>
                            <div class="block block-four"></div>
                                @if($profile->image)
                                <img class="avatar" src="{{asset($profile->image) }}" >
                                @else
                                <img class="avatar" src="{{ asset('assets/img/default-avatar.png') }}" alt="">
                                @endif

                                <h5 class="title">{{ auth()->user()->name }}</h5>

                        </div>
                    </p>

                </div>

                {{-- <div class="card-footer">
                    <div class="button-container">
                        <button class="btn btn-icon btn-round btn-facebook">
                            <i class="fab fa-facebook"></i>
                        </button>
                        <button class="btn btn-icon btn-round btn-twitter">
                            <i class="fab fa-twitter"></i>
                        </button>
                        <button class="btn btn-icon btn-round btn-google">
                            <i class="fab fa-google-plus"></i>
                        </button>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h4>Change Password</h4>
            </div>
            <form method="post" action="{{url('/admin/profile/changepassword/'.$profile->id)}}" autocomplete="off">
                <div class="card-body">
                    @csrf
                    <div class="form-group">
                        <label>Current password</label>
                        <input type="password" name="current_password" class="form-control" placeholder="Current password" value="" required>
                        @error('current_password')
                            <span class="text-danger">*{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label>New Password</label>
                        <input type="password" name="new_password" class="form-control" placeholder="New password" value="" required>
                        @error('new_password')
                            <span class="text-danger">*{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Confirm new password</label>
                        <input type="password" name="confirm_password" class="form-control" placeholder="Confirm new password" value="" required>
                        @error('confirm_password')
                            <span class="text-danger">*{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-fill btn-primary">Change Password</button>
                </div>
            </form>
        </div>
      </div>
    </div>
@endsection
