@extends('admin.layout.master')
@section('container')

@php $total_contact = App\Contact::count(); @endphp



<div class="row">
    <div class="col-xl-3 col-lg-6">
      <div class="card card-stats mb-4 mb-xl-0">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <a href="{{ route('contact.index') }}"><h5 class="text-uppercase mb-2">Contact</h5></a>
                    <span class="text-primary h2 font-weight-bold mb-0">{{ $total_contact }}</span>
                </div>
                <div class="col-auto">
                    <div class="icon icon-shape shadow">
                        <h3><i class="tim-icons icon-mobile"></i></h3>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>


@endsection

