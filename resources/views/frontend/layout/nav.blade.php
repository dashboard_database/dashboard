<header class="section rd-navbar-wrap" id="home" data-preset='{"title":"Navbar Default","category":"header","reload":true,"id":"navbar-default"}'>
    <nav class="rd-navbar">
      <div class="navbar-container">
        <div class="navbar-cell">
          <div class="navbar-panel">
            <button class="navbar-switch mdi-menu novi-icon" data-multi-switch='{"targets":".rd-navbar","scope":".rd-navbar","isolate":"[data-multi-switch]"}'></button>
            <div class="navbar-logo">
                <a class="navbar-logo-link" href="#home">
                    <p> <b style="font-size: 18px;color: #112950;font-weight: bolder;">Dr. Malay H Joshi</b> <br><b style="font-size: 11px;color: #112950;font-weight: bolder;">[Homoeopathic Physician]</b></p>

                    {{-- <img class="navbar-logo-default" src="images/logo-default-260x100.png" alt="Component Trunk" width="130" height="50"/> --}}
                    {{-- <img class="navbar-logo-inverse" src="images/logo-inverse-260x100.png" alt="Component Trunk" width="130" height="50"/> --}}
                </a>
            </div>
          </div>
        </div>
        <div class="navbar-cell navbar-spacer"></div>
        <div class="navbar-cell navbar-sidebar">
          <ul class="navbar-navigation rd-navbar-nav">
            <li class="navbar-navigation-root-item active"><a class="navbar-navigation-root-link" href="#home">Home</a>
            </li>
            <li class="navbar-navigation-root-item"><a class="navbar-navigation-root-link" href="#about">About</a>
            </li>
            <li class="navbar-navigation-root-item"><a class="navbar-navigation-root-link" href="#services">Services</a>
            </li>
            {{-- <li class="navbar-navigation-root-item"><a class="navbar-navigation-root-link" href="#reviews">Reviews</a>
            </li> --}}
            <li class="navbar-navigation-root-item"><a class="navbar-navigation-root-link" href="#contacts">Contacts</a>
            </li>
          </ul>
        </div>
        {{-- <div class="navbar-cell">
          <div class="navbar-subpanel">
            <div class="navbar-subpanel-item">
              <div class="navbar-info">
                  <a href="#" target="_blank">
                    <button class="btn"><span class="btn-icon mdi-book-open"></span><span>Make an Appointment</span></button>
                  </a>
              </div>
              <button class="navbar-button navbar-info-button mdi-dots-vertical novi-icon" data-multi-switch='{"targets":".rd-navbar","scope":".rd-navbar","class":"navbar-info-active","isolate":"[data-multi-switch]"}'></button>
            </div>
          </div>
        </div> --}}
      </div>
    </nav>
  </header>
