


<!DOCTYPE html>
<html lang="en">

    @include('frontend.layout.head')

    <body>
        <div class="page">

            @include('frontend.layout.nav')
            @section('container')

            @show
        </div>
        <!-- Preloader-->
        <div class="preloader">
            <div class="preloader-inner">
            <div class="preloader-item"></div>
            <div class="preloader-item"></div>
            <div class="preloader-item"></div>
            <div class="preloader-item"></div>
            <div class="preloader-item"></div>
            <div class="preloader-item"></div>
            <div class="preloader-item"></div>
            <div class="preloader-item"></div>
            <div class="preloader-item"></div>
            </div>
        </div>

        @include('frontend.layout.script')
    </body>
</html>
