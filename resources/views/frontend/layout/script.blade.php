
<!-- Global site tag (gtag.js) - Google Analytics -->
{{-- <script async src="https://www.googletagmanager.com/gtag/js?id=G-5F3G241VV3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5F3G241VV3');
</script> --}}



<script src="{{asset('components/base/core.min.js')}}"></script>
<script src="{{asset('components/base/script.js')}}"></script>

<script src="{{asset('components/izitoast/js/iziToast.min.js')}}"></script>

            <script>
                @if(Session::has('success'))
                    iziToast.success({
                    title: '{{ session('success') }}',
                    position: 'topRight'
                });
                @endif
                @if(Session::has('error'))
                    iziToast.error({
                    title: '{{ session('error') }}',
                    position: 'topRight'
                });
                @endif
            </script>
