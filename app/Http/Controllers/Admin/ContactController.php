<?php




namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function index()
    {
        $contacts = Contact::get();
        return view('admin.contact.index',compact('contacts'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Contact $contact)
    {
        //
    }

    public function edit(Contact $contact)
    {
        //
    }

    public function update(Request $request, Contact $contact)
    {
        //
    }

    public function destroy($id)
    {
        Contact::find($id)->delete();
        return back()->with('success','Contact Deleted Successfully!');
    }
}
