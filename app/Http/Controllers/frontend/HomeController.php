<?php



namespace App\Http\Controllers\frontend;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Illuminate\Support\Facades\Session as FacadesSession;

class HomeController extends Controller
{


    public function index()
    {
        return view('frontend.index');
    }
    public function insert(Request $request)
    {
        $request->validate([
            'name' => 'bail|required',
            'email' => 'bail|required',
            'contact' => 'bail|required',
        ]);

        $contact = new Contact();
        $contact->name = $request->input('name');
        $contact->email = $request->input('email');
        $contact->contact = $request->input('contact');
        $contact->subject = $request->input('subject');
        $contact->message = $request->input('message');
        $status = $contact->save();
        if($status){
            return back()->with('success','Thank you for contacting us');
        }
        else{
            return back()->with('error','Oops! Something Went wrong!');
        }
    }

}
