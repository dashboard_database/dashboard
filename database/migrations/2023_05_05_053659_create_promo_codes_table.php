<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_codes', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('title')->default(null)->nullable();
            $table->text('description')->default(null)->nullable();
            $table->float('min_amount')->default(0);
            $table->float('max_discount_amount')->default(0);
            $table->float('discount_amount')->default(0);
            $table->tinyInteger('discount_type')->default(1)->comment('1 => flat amount, 2 => percentage');
            $table->tinyInteger('status')->default(1)->comment('1 => Active, 0 => Inactive');
            $table->timestamp('start_date')->nullable()->default(null);
            $table->timestamp('end_date')->nullable()->default(null);
            $table->timestamp('per_user_limit')->nullable()->default(null);
            $table->tinyInteger('type')->default(1)->comment('1 => promo code, 2 => referral code');
            $table->foreignId('user_id')->nullable()->default(null)->constrained('users')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_codes');
    }
}
