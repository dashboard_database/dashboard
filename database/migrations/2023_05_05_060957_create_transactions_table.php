<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('booking_id')->nullable()->default(null)->constrained('bookings')->cascadeOnDelete();
            $table->float('amount')->default(0);
            $table->tinyInteger('transaction_type')->default(1)->comment('1 => cod,2 => upi,3 => card');
            $table->tinyInteger('transaction_status')->default(1)->comment('2 => Cancelled,1 => Completed, 0 => Inprogress');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
