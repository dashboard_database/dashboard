<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTypeDurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_type_durations', function (Blueprint $table) {
            $table->id();
            $table->string('title')->default(null)->nullable();
            $table->text('description')->default(null)->nullable();
            $table->integer('display_order')->default(null)->nullable();
            $table->time('start_time')->default(null)->nullable();
            $table->time('end_time')->default(null)->nullable();
            $table->tinyInteger('status')->default(1)->comment('1 => Active, 0 => Inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_type_durations');
    }
}
