<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->foreignId('car_type_id')->constrained('car_types')->cascadeOnDelete();
            $table->foreignId('driver_id')->nullable()->default(null)->constrained('drivers')->cascadeOnDelete();
            $table->foreignId('car_id')->nullable()->default(null)->constrained('cars')->cascadeOnDelete();
            $table->foreignId('service_id')->nullable()->default(null)->constrained('services')->cascadeOnDelete();
            $table->foreignId('promocode_id')->nullable()->default(null)->constrained('promo_codes')->cascadeOnDelete();
            $table->timestamp('start_date')->nullable()->default(null);
            $table->timestamp('end_date')->nullable()->default(null);
            $table->timestamp('shedule_pickup_date')->nullable()->default(null);
            $table->string('pickup_point')->nullable()->default(null);
            $table->string('pickup_latitude')->nullable()->default(null);
            $table->string('pickup_longitude')->nullable()->default(null);
            $table->string('dropoff_point')->nullable()->default(null);
            $table->string('dropoff_latitude')->nullable()->default(null);
            $table->string('dropoff_longitude')->nullable()->default(null);
            $table->text('note')->nullable()->default(null);
            $table->tinyInteger('status')->default(1)->comment('1 => Booked, 2 => Confirmed,3 => Cancelled by user,4 => Cancelled by driver,5 => Completed');
            $table->float('promocode_discount_value')->default(0);
            $table->tinyInteger('promocode_discount_type')->default(1)->comment('1 => flat amount, 2 => percentage');
            $table->float('tax_value')->default(0);
            $table->tinyInteger('tax_type')->default(1)->comment('1 => flat amount, 2 => percentage');
            $table->float('subtotal_amount')->default(0);
            $table->float('total_discount_amount')->default(0);
            $table->float('tax_amount')->default(0);
            $table->float('total_amount')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
