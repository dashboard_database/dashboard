<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
// use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::view('/admin/login','admin.auth.login');
Auth::routes();
Route::get('/logout', 'Admin\HomeController@logout')->name('logout');
Route::group(['prefix' => '/admin', 'middleware'=>['auth']],function(){

    // Dashboard
    Route::get('/dashboard', 'Admin\HomeController@index')->name('dashboard');
    // contact
    Route::resource('/contact','Admin\ContactController');
    //profile
    Route::get('/profile', 'Admin\AdminProfileController@profile')->name('admin-profile');
    Route::post('/profile/update/{id}', 'Admin\AdminProfileController@update')->name('admin-profile-update');
    Route::post('/profile/changepassword/{id}','Admin\AdminProfileController@changePassword');
});
Route::get('/', 'frontend\HomeController@index');
Route::post('/contactus', 'frontend\HomeController@insert');
